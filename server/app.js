var express = require("express");
var app = express();

console.log(__dirname);
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);
app.use(express.static(__dirname + "/../client/"));

var students = [
    {
        id: 1,
        name: "Kenneth",
        age:25
    },
    {
        id: 2,
        name: "Alex",
        age: 26
    }
];

var y = 5;

app.get("/students", (req,res)=>{
    console.log("I am in students endpoint");
    res.status(200).json(students);
});

app.get("/students/:student_id", (req,res)=>{
    console.log(req.params.student_id);
    let studentId = req.params.student_id;
    let studentObj = null;
    for(var x =0 ; x < students.length; x++){
        if(students[x].id == studentId){
            studentObj = students[x];
            break;
        }
    }
    res.status(200).json(studentObj);
});

app.use((req,res,next)=>{
    console.log(" y +2");
    y = y +2;
    next();
});

app.use((req,res,next)=>{
    console.log("step 3");
    y = y +8;
    next();
});

app.use((req,res,next)=>{
    console.log(" step 4");
    y = y +13;
    next();
});

app.use((req,res,next)=>{
    console.log(" y +3");
    y = y +5;
    next();
});

app.use((req,res)=>{
    var x = 6;
    try{
        console.log("Y : " + y);
        res.send(`<h1> Oopps wrong please ${y}</h1>`);
    }catch(error){
        console.log(error);
        res.status(500).send("ERROR");
    } 
});

app.listen(NODE_PORT, ()=>{
    console.log(`Web App started at ${NODE_PORT}`);
});